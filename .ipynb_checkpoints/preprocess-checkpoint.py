#%%
import pandas as pd
import numpy as np

""" def clean_horse(df):
    df2 = df[df.location != "從化"]
    df3 = pd.concat([df2, df2[df2.apply(lambda r: r.str.contains("---", case=False).any(), axis=1)]]).drop_duplicates(keep=False) 
    
    return df3

df = pd.read_csv("raw_data.csv")
df = clean_horse(df)
df.to_csv("cleaned_data.csv") """

# %%
horse_data = pd.read_csv('horse_data.csv', header=0)
horse_data_2 = pd.read_csv('horse_data_2.csv', header=0)
horse_data_3 = pd.read_csv('horse_data_3.csv', header=0)

# %%
horse_data_full = pd.concat([horse_data, horse_data_2, horse_data_3])
horse_data_full.reset_index(drop=True, inplace=True)
horse_data_full.drop(['0', '11', '17', '18', '20', '21', '22'], axis=1, inplace=True)
horse_data_full.rename(columns={'1': 'place', '2': 'date', '3': 'track', \
                        '4': 'distance', '5': 'going', '6': 'race_class', \
                        '7': 'draw', '8': 'rating', '9': 'trainer', \
                        '10': 'jockey', '12': 'win_odds', \
                        '13':'actual_weight'}, inplace=True)
horse_data_full.drop(['trainer', 'jockey'], axis=1, inplace=True)

# %%
horse_data_full = horse_data_full[~horse_data_full['place'].isin(['WV-A', 'WV', 'DISQ', 'WX-A', 'UR', 'WX', 'WXNR', 'PU', 'TNP', 'DNF'])]
num_win = horse_data_full[horse_data_full['place'].astype(int) == 1].groupby(['horse_id']).size()
num_win = num_win.to_frame()
total_num_race = horse_data_full.groupby(['horse_id']).count()['place']
total_num_race = total_num_race.to_frame()

horse_data_full = horse_data_full[~horse_data_full['rating'].isin(['--'])]
horse_data_full['rating'] = horse_data_full['rating'].astype(int)
avg_rating = horse_data_full.groupby(['horse_id']).mean()['rating']
avg_rating = avg_rating.to_frame()

merged = total_num_race.merge(right=avg_rating, how='left', left_index=True, right_index=True)
merged = merged.merge(right=num_win, how='left', left_index=True, right_index=True)
merged.rename(columns={'place': 'horse_num_race', 'rating': 'horse_rating', 0: 'horse_num_win'}, inplace=True)
merged['horse_pct_win'] = merged['horse_num_win']/merged['horse_num_race']


# %%
trainer = pd.read_csv('trainer.csv', header=0, index_col=0)
trainer = trainer.iloc[0:22, :]
trainer['train_num_win'] = trainer['冠']
trainer['train_pct_win'] = trainer['冠']/trainer['出馬總數']
trainer.drop(['冠',	'亞', '季', '殿', '第五', '出馬總數', '所贏獎金'], axis=1, inplace=True)

# %%
jockey = pd.read_csv('jockey.csv', header=0, index_col=0)
jockey = jockey.iloc[0:29, :]
jockey['jockey_num_win'] = jockey['冠']
jockey['jockey_pct_win'] = jockey['冠']/jockey['總出賽次數']
jockey.drop(['冠', '亞', '季', '殿', '第五', '總出賽次數', '所贏獎金'], axis=1, inplace=True)


# %%
race_data = pd.read_csv('cleaned_data.csv', header=0)
race_data.drop(['Unnamed: 0', 'Unnamed: 0.1'], axis=1, inplace=True)
race_data.drop(['horse_no', 'lbw', 'finish_time', 'going', 'course'], axis=1, inplace=True)

race_data['horse_id'] = race_data['horse'].str.replace('[^0-9A-Za-z]+', '')
race_data['horse'] = race_data['horse'].str.replace('[\(0-9\)A-Za-z]+', '')

race_data['track'] = race_data['location'] + race_data['length']
race_data.drop(['location', 'length'], axis=1, inplace=True)

race_data['newdist'] = 0


# for horse in race_data['horse_id'].unique():
#     temp = race_data[race_data.horse_id == horse]
#     temp_index = temp.index
#     temp_track = temp['track'].unique()

#     idx_newdist = []
#     for t in temp_track:
#         idx_newdist.append((temp['track'] == t)[::-1].idxmax())
#     race_data.loc[idx_newdist, 'newdist'] = 1
# # %%

# race_data = race_data.merge(right=merged, how='left', left_on='horse_id', right_index=True)
# race_data.dropna(inplace=True)

# race_data = race_data.merge(right=jockey, how='left', left_on='jockey', right_on='騎師')
# race_data.drop(['騎師'], axis=True, inplace=True)
# race_data[['jockey_num_win', 'jockey_pct_win']] = race_data[['jockey_num_win', 'jockey_pct_win']].fillna(value=0)
# race_data['jockey_miss_data'] = np.where(race_data['jockey_num_win'] == 0, 1, 0)

# race_data = race_data.merge(right=trainer, how='left', left_on='trainer', right_on='練馬師')
# race_data.drop(['練馬師'], axis=True, inplace=True)
# race_data[['train_num_win', 'train_pct_win']] = race_data[['train_num_win', 'train_pct_win']].fillna(value=0)
# race_data['train_miss_data'] = np.where(race_data['train_num_win'] == 0, 1, 0)

# race_data.reset_index(drop=True, inplace=True)
# # %%
# Z = race_data[['plc', 'win_odds', 'horse_pct_win', 'horse_rating', 'horse_num_win', 'actual_wt', 'draw', 'newdist', 'jockey_pct_win', 'jockey_num_win', 'jockey_miss_data', 'train_num_win', 'train_pct_win', 'train_miss_data']]
# Z.to_csv('completed_data.csv')




# %%
race_data = race_data.merge(right=merged, how='left', left_on='horse_id', right_index=True)
#race_data.dropna(inplace=True)

race_data = race_data.merge(right=jockey, how='left', left_on='jockey', right_on='騎師')
race_data.drop(['騎師'], axis=True, inplace=True)
race_data[['jockey_num_win', 'jockey_pct_win']] = race_data[['jockey_num_win', 'jockey_pct_win']].fillna(value=0)
race_data['jockey_miss_data'] = np.where(race_data['jockey_num_win'] == 0, 1, 0)

race_data = race_data.merge(right=trainer, how='left', left_on='trainer', right_on='練馬師')
race_data.drop(['練馬師'], axis=True, inplace=True)
race_data[['train_num_win', 'train_pct_win']] = race_data[['train_num_win', 'train_pct_win']].fillna(value=0)
race_data['train_miss_data'] = np.where(race_data['train_num_win'] == 0, 1, 0)

race_data.reset_index(drop=True, inplace=True)

# %%
race_data['race_num'] = np.NaN
race_data.loc[race_data['plc'] == 1, 'race_num'] = range(1, race_data['plc'].value_counts().iloc[0]+1)
race_data.fillna(method='ffill', inplace=True)
race_data.drop(['horse', 'jockey', 'trainer', 'horse_num_race'], axis=1, inplace=True)
race_data.set_index(keys = ["race_num", "plc"], inplace=True)

# %%
new_race_data = pd.DataFrame()
for race_num, df in race_data.groupby(level = 0):
    df['relative_actual_wt'] = df['actual_wt']/df['actual_wt'].sum()
    df['relative_declar_horse_wt'] = df['declar_horse_wt']/df['declar_horse_wt'].sum()
    df['relative_win_odds'] = df['win_odds']/df['win_odds'].sum()
    df['relative_horse_rating'] = df['horse_rating']/df['horse_rating'].sum()
    df['relative_horse_num_win'] = df['horse_num_win']/df['horse_num_win'].sum()
    df['relative_pct_win'] = df['horse_pct_win']/df['horse_pct_win'].sum()
    df['relative_jockey_num_win'] = df['jockey_num_win']/df['jockey_num_win'].sum()
    df['relative_jockey_pct_win'] = df['jockey_pct_win']/df['jockey_pct_win'].sum()
    df['relative_train_num_win'] = df['train_num_win']/df['train_num_win'].sum()
    df['relative_train_pct_win'] = df['train_pct_win']/df['train_pct_win'].sum()

    df.drop(['actual_wt', 'declar_horse_wt', 'horse_id', 'track', 'horse_rating', 'horse_num_win', 'horse_pct_win', 'jockey_num_win', 'jockey_pct_win', 'train_num_win', 'train_pct_win'], axis=1, inplace=True)

    new_race_data = pd.concat([new_race_data, df])

new_race_data.to_csv('relative_data.csv')
# %%
