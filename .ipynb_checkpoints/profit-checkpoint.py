import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

class Profit():
    
    def __init__(self, model, X, y, win_odds, proba):
        
        if proba == True:
            # add kelly criterion
            return self.proba_profit(model, X, y, win_odds)
        else:
            return self.profit(model, X, y, win_odds)
        
    def proba_profit(self, model, X, y, win_odds):

        proba = model.predict_proba(X)
        proba = proba[:, 0] # getting 1st place
        
        y.reset_index(drop=True, inplace=True)
        won = y.loc[y == 1]
        series = pd.Series(-1, y.index)
        series.loc[won.index] = won.values
        series = series * proba
        series = series.cumsum()
        
        plot = series.plot()
        fig = plot.get_figure()
        fig.savefig("output.png")
        
    def profit(self, model, X, y, win_odds):
        equal = y.loc[y == model.predict(X)]
        equal = equal.loc[equal == 1]
        series = pd.Series(-1, y.index)
        won = win_odds[equal.index]
        series.loc[won.index] = won.values
        
        series = series.reset_index(drop=True).cumsum()
        
        plot = series.plot()
        fig = plot.get_figure()
        fig.savefig("output.png")