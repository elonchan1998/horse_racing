import pandas as pd
import numpy as np

from sklearn.model_selection import train_test_split
from sklearn.metrics import log_loss, accuracy_score

from profit import Profit

data = pd.read_csv('relative_data.csv', header=0)
data.drop(['race_num'], axis=1, inplace=True)
data.dropna(inplace=True)

data = data.loc[data.plc <= 8 ]

y = data['plc']
X = data.drop(['plc'], axis=1)

# train-valid-test split
train_X, test_X, train_y, test_y = train_test_split(X, y, test_size=0.2, random_state=1)
train_X, valid_X, train_y, valid_y = train_test_split(train_X, train_y, test_size=0.25, random_state=1) # 0.25 x 0.8 = 0.2

# Drop win-odds
train_win_odds = train_X['win_odds']
train_X.drop(['win_odds'], axis=1, inplace=True)
valid_win_odds = valid_X['win_odds']
valid_X.drop(['win_odds'], axis=1, inplace=True)
test_win_odds = test_X['win_odds']
test_X.drop(['win_odds'], axis=1, inplace=True)


def metric(model, X, y, proba=False):
    if proba == True:
        return accuracy_score(y, model.predict_proba(X))
    else:
        return accuracy_score(y, model.predict(X))


print('\n', 'Gaussian Native Bayes')
from sklearn.naive_bayes import *
gnb = GaussianNB()
gnb.fit(train_X, train_y)

print('Training: ', metric(gnb, train_X, train_y))
print('Valid: ', metric(gnb, valid_X, valid_y))
print('Testing: ', metric(gnb, test_X, test_y))

# pf = Profit(gnb, test_X, test_y, test_win_odds, proba=True)


print('\n', 'Linear Discriminant Analysis')
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
lda = LinearDiscriminantAnalysis()
lda.fit(train_X, train_y)

print('Training: ', metric(lda, train_X, train_y))
print('Valid: ', metric(lda, valid_X, valid_y))
print('Testing: ', metric(lda, test_X, test_y))
# pf = Profit(lda, test_X, test_y, test_win_odds, proba=True)


print('\n', 'Support Vector Machine Classifier')
from sklearn.svm import SVC
svc = SVC(probability=True)
svc.fit(train_X, train_y)

print('Training: ', metric(svc, train_X, train_y))
print('Valid: ', metric(svc, valid_X, valid_y))
print('Testing: ', metric(svc, test_X, test_y))
pf = Profit(svc, test_X, test_y, test_win_odds, proba=True)


print('\n', 'Decision Tree')
from sklearn import tree
dt = tree.DecisionTreeClassifier(max_depth=5)
dt.fit(train_X, train_y)

print('Training: ', metric(dt, train_X, train_y))
print('Valid: ', metric(dt, valid_X, valid_y))
print('Testing: ', metric(dt, test_X, test_y))

#Export tree structure to PNG format
# from sklearn.externals.six import StringIO  
# import pydot
# dotfile = StringIO()
# tree.export_graphviz(dt, out_file=dotfile)
# pydot.graph_from_dot_data(dotfile.getvalue())[0].write_png("tree.png")


print('\n', 'K Nearest Neigbhor')
from sklearn.neighbors import KNeighborsClassifier
#Only consider the nearest neighbor
knn = KNeighborsClassifier(n_neighbors=5)
knn.fit(train_X, train_y)

print('Training: ', metric(knn, train_X, train_y))
print('Valid: ', metric(knn, valid_X, valid_y))
print('Testing: ', metric(knn, test_X, test_y))


print('\n', 'Random Forest')
from sklearn.ensemble import RandomForestClassifier
rf = RandomForestClassifier()
rf.fit(train_X, train_y)

print('Training: ', metric(rf, train_X, train_y))
print('Valid: ', metric(rf, valid_X, valid_y))
print('Testing: ', metric(rf, test_X, test_y))

