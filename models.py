import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.optim import Adam
from torch.utils.tensorboard import SummaryWriter

import pandas as pd
import numpy as np
from sklearn.model_selection import train_test_split

data = pd.read_csv('relative_data.csv', header=0)
data.drop(['race_num'], axis=1, inplace=True)
data.dropna(inplace=True)

y = data['plc']
X = data.drop(['plc'], axis=1)

# train-valid-test split
train_X, test_X, train_y, test_y = train_test_split(X, y, test_size=0.2, random_state=1)
train_X, valid_X, train_y, valid_y = train_test_split(train_X, train_y, test_size=0.25, random_state=1) # 0.25 x 0.8 = 0.2

# Hyperparameters ANN
input_dim = train_X.shape[1]
hidden_dim = 256
output_dim = 14 
lr = 0.00001
dropout = 0
epochs = 50

batch_size = 32
def format_X(x, batch_size):
    x = x.head(-(x.shape[0]%batch_size))
    x = np.array(x)
    x = np.reshape(x, (-1, batch_size, x.shape[1]))
    return torch.cuda.FloatTensor(x)

def format_y(y, batch_size):
    y = y.head(-(y.shape[0]%batch_size))
    y = np.array(y)
    y = np.reshape(y, (-1, batch_size))
    return torch.cuda.LongTensor(y)

train_X = format_X(train_X, batch_size)
train_y = format_y(train_y, batch_size)

class ANN(nn.Module):
    def __init__(self, input_dim, hidden_dim, output_dim, dropout):
        super().__init__()
        self.input = nn.Linear(input_dim, hidden_dim)
        self.hidden = nn.Linear(hidden_dim, hidden_dim)
        self.output = nn.Linear(hidden_dim, output_dim)
        
        self.dropout = nn.Dropout(p=dropout)
        
    def forward(self, sample):
        input = self.input(sample)
        print(input.shape)
        hidden = F.softmax(self.hidden(input), dim = 1)
        hidden = F.softmax(self.hidden(hidden), dim = 1)
        hidden = F.softmax(self.dropout(hidden), dim = 1)
        hidden = F.softmax(self.hidden(hidden), dim = 1)
        output = self.output(hidden)
        
        return output
    
model = ANN(input_dim, hidden_dim, output_dim, dropout)
model.cuda()

#criterion = nn.CrossEntropyLoss()
optimizer = Adam(model.parameters(), lr=lr)

writer = SummaryWriter('runs/exp-1')
writer.add_graph(model, train_X[0])

train_length = len(train_X)
for epoch in range(epochs):
    model.train()
    
    for batch_i, (X, y) in enumerate(zip(train_X, train_y)):
        optimizer.zero_grad()
        #print(X)
        #print(X.shape)
        outputs = model(X)
        #print(outputs)
        #print(outputs.shape)
        
        #print(y)
        #print(y.shape)
        loss = F.cross_entropy(outputs, y)
        loss.backward()
        optimizer.step()
        break
        writer.add_scalar('training loss', loss.item(), epoch * train_length + batch_i)
        
    break
        
print('training_finished')

for parameter in model.parameters():
    print(parameter)
    

