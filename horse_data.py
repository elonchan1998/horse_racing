# %%
import pandas as pd
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support.expected_conditions import presence_of_element_located


# %%
df = pd.read_csv('cleaned_data.csv')
df = df[['horse']]
horse_id = df['horse'].str.replace('[^0-9A-Za-z]+', '')
horse_id = horse_id.unique().tolist()
horse = df['horse'].str.replace('[\(0-9\)A-Za-z]+', '')
horse = horse.unique().tolist()

# %%
url = r'https://racing.hkjc.com/racing/information/chinese/Horse/SelectHorse.aspx'

op = webdriver.ChromeOptions()
op.add_argument('headless')

driver = webdriver.Chrome(options=op)
wait = WebDriverWait(driver, 1)

df_all_horse = pd.DataFrame()

# %%
for i, horse in enumerate(horse_id_3):
    j = i + 1246
    driver.get(url)
    try:
        wait.until(presence_of_element_located((By.XPATH, "//input[@name='BrandNumber']")))
        driver.find_elements(By.XPATH, "//input[@name='BrandNumber']")[0].send_keys(horse + Keys.RETURN)
    except Exception as e:
        print(e)
        print(j, ' ', horse, 'Not in public database anymmore')
        continue

    try:
        wait.until(presence_of_element_located((By.XPATH, "//a[img/@alt='所有往績']")))
        driver.find_element_by_xpath("//a[img/@alt='所有往績']").click()
    except Exception as e:
        print(e)
        print(j, ' ', horse, '已退役馬默認為所有往績')
        pass

    try:
        results = wait.until(presence_of_element_located((By.XPATH, "//table[@class='bigborder']")))
        results = results.find_elements_by_tag_name('tr')
    except Exception as e:
        print(e)
        print(j, ' ', 'Sth happens during horse ', horse)

    #header = results[0].text.replace('\n', '').split()
    #header = header[:-2]
    #header.remove('沿途走位')

    table = []
    for result in results[1:]:
        text = result.text
        if len(text) < 20: # skip subheader row
            continue

        text = text.split()
        table.append(text)

    df_horse = pd.DataFrame.from_dict(table)
    df_horse.drop([14, 15, 16, 19], axis=1, inplace=True)
    df_horse['horse_id'] = horse
    df_horse.set_index(['horse_id'], inplace=True)

    df_all_horse = pd.concat([df_all_horse, df_horse], axis=0)

    if j % 10 == 0:
        print('Saved until ', j)
        df_all_horse.to_csv('horse_data_3.csv')


# %%
driver.quit()

# %%
