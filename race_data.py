#%%
from selenium import webdriver
import re
import pandas as pd
from time import sleep

#%%
def get_racing_days():
    url = "https://racing.hkjc.com/racing/information/English/Racing/LocalResults.aspx"
    browser = webdriver.Chrome()
    browser.get(url)

    class_name = "f_fs11"
    html_class = browser.find_elements_by_class_name(class_name)
    results = html_class[0].text

    return re.findall(r"\d+/\d+/\d+", results)


#%%
def get_data(racing_days):
    browser = webdriver.Chrome()

    df = pd.DataFrame(columns=["plc", "horse_no", "horse", "jockey", "trainer", "actual_wt",
                            "declar_horse_wt", "draw", "lbw", "finish_time", "win_odds",
                            "location", "length", "going", "course"])

    skip_count = 0 
    k = 0
    for date in racing_days[1:]:
        for race in range(1,13):
            day = date[0:2]
            month = date[3:5]
            year = date[6:10]

            url = "https://racing.hkjc.com/racing/information/Chinese/Racing/LocalResults.aspx?RaceDate=" + \
                str(year) + "/" + str(month) + "/" + str(day) + "&RaceNo=" + str(race)
            
            print(url)
            
            browser.get(url)

            try:
                class_name = r"f_fs12 fontFam"
                html_class = browser.find_elements_by_css_selector(".f_fs12.fontFam")
                location = browser.find_elements_by_class_name("font_w7")[0].text[:-1]
                print("location", location, end="")
                race_tab = browser.find_elements_by_class_name("race_tab")[0].text
                print("race_tab", end="")
                length = re.findall(r"\d+米", race_tab)[0]
                print("length", length, end="")
                going = re.findall(r"(場地狀況 : .+)\n", race_tab)[0][7:]
                print("going", going, end="")
                course = re.findall(r"(賽道 : .+) -", race_tab)[0][5:]
                print("course", course)
                results = html_class[0].text
            except Exception as e:
                print(str(e))
                print("Skipped: ", url)
                continue

            results = re.sub(r"\n((\d{,2} +\d+)|(---))+\n", " ", results)
            results = re.sub(r"\n", " ", results)
            results = re.sub(r"平頭馬 ", "", results)
            results = results.split(" ")
            for i in range(0, len(results), 11):
                try: 
                    plc = int(results[0+i])
                    horse_no = results[1+i]
                    horse = results[2+i]
                    jockey = results[3+i]
                    trainer = results[4+i]
                    actual_wt = results[5+i]
                    declar_horse_wt = results[6+i]
                    draw = results[7+i]
                    lbw = results[8+i]
                    finish_time = results[9+i]
                    win_odds = results[10+i]
                except Exception as e:
                    skip_count += 1
                    print(e)
                    continue
                to_append = [plc, horse_no, horse, jockey, trainer, actual_wt, declar_horse_wt, draw,
                            lbw, finish_time, win_odds, location, length, going, course]
                df = df.append(pd.Series(to_append, index = df.columns), ignore_index = True)
                print("k th: ", k, "record:", plc, horse, jockey)
                k += 1
                if k%100 == 0:
                    sleep(1)
                    print(k)
    print("Skip Count: " ,skip_count)
    return df 

if __name__ == "__main__":
    racing_days = get_racing_days()
    get_data(racing_days)
    df.to_csv("raw_data.csv")