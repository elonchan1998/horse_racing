# %%
import pandas as pd
from selenium import webdriver


# %%
if __name__ == '__main__':
    df = pd.read_csv('cleaned_data.csv')
    df = df[['horse', 'jockey', 'trainer']]
    horse_id = df['horse'].str.replace('[^0-9A-Za-z]+', '')
    horse_id = horse_id.unique().tolist()
    horse = df['horse'].str.replace('[\(0-9\)A-Za-z]+', '')
    horse = horse.unique().tolist()
    jockey = df['jockey']
    jockey = jockey.unique().tolist()
    trainer = df['trainer']
    trainer = trainer.unique().tolist()

# %%
op = webdriver.ChromeOptions()
op.add_argument('headless')
driver = webdriver.Chrome()

url = r'https://racing.hkjc.com/racing/information/Chinese/Jockey/JockeyRanking.aspx'

driver.get(url)
headers = driver.find_elements_by_class_name('Ranking')

jockeys_info = headers[0].text

jockeys_info = jockeys_info.split('\n')
jockeys_info = jockeys_info[8:]
jockeys_info.remove('其他')
jockeys_info.remove('在港現役騎師')


table = []

for row in jockeys_info:
    table.append(row.split(' '))

header = table.pop(0)

df_jockey = pd.DataFrame(table, columns=header)
# %%
url = r'https://racing.hkjc.com/racing/information/chinese/Trainers/TrainerRanking.aspx'

driver.get(url)
headers = driver.find_elements_by_class_name('Ranking')
trainers_info = headers[0].text
driver.close()

trainers_info = trainers_info.split('\n')
trainers_info = trainers_info[8:]
trainers_info.remove('其他')
trainers_info.remove('在港現役練馬師')


table = []

for row in trainers_info:
    table.append(row.split(' '))

header = table.pop(0)

df_trainer = pd.DataFrame(table, columns=header)


# %%
