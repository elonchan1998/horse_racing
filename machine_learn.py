# %%
import pandas as pd
import numpy as np

from sklearn.model_selection import train_test_split
from sklearn.metrics import log_loss, accuracy_score, precision_score, recall_score, f1_score

from profit import Profit

# %%
data = pd.read_csv('relative_data.csv', header=0)
data.drop(['race_num'], axis=1, inplace=True)
data.dropna(inplace=True)

data = data.loc[data.plc <= 8 ]

# %%
y = data['plc']
X = data.drop(['plc'], axis=1)

# train-valid-test split
train_X, test_X, train_y, test_y = train_test_split(X, y, test_size=0.2, random_state=1)
train_X, valid_X, train_y, valid_y = train_test_split(train_X, train_y, test_size=0.25, random_state=1) # 0.25 x 0.8 = 0.2

# Drop win-odds
train_win_odds = train_X['win_odds']
train_X.drop(['win_odds'], axis=1, inplace=True)
valid_win_odds = valid_X['win_odds']
valid_X.drop(['win_odds'], axis=1, inplace=True)
test_win_odds = test_X['win_odds']
test_X.drop(['win_odds'], axis=1, inplace=True)


def metric(model, X, y, proba=False):
    if proba == True:
        print(f1_score(y, model.predict_proba(X), average='weighted'))
    else:
        print(accuracy_score(y, model.predict(X)))
        print(precision_score(y, model.predict(X), average='macro'))
        print(recall_score(y, model.predict(X), average='macro'))
        print(f1_score(y, model.predict(X), average='macro'))


# print('\n', 'Gaussian Native Bayes')
# from sklearn.naive_bayes import *
# gnb = GaussianNB()
# gnb.fit(train_X, train_y)

# print('Training: ', metric(gnb, train_X, train_y))
# print('Valid: ', metric(gnb, valid_X, valid_y))
# print('Testing: ', metric(gnb, test_X, test_y))

# pf = Profit(gnb, test_X, test_y, test_win_odds, proba=True)

# print('\n', 'Linear Discriminant Analysis')
# from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
# lda = LinearDiscriminantAnalysis()
# lda.fit(train_X, train_y)

# # print('Training: ', metric(lda, train_X, train_y))
# # print('Valid: ', metric(lda, valid_X, valid_y))
# print('Testing: ', metric(lda, test_X, test_y))
# pf = Profit(lda, test_X, test_y, test_win_odds, proba=True)


# print('\n', 'Support Vector Machine Classifier')
# from sklearn.svm import SVC
# svc = SVC(probability=True)
# svc.fit(train_X, train_y)

# # print('Training: ', metric(svc, train_X, train_y))
# # print('Valid: ', metric(svc, valid_X, valid_y))
# print('Testing: ', metric(svc, test_X, test_y))
# pf = Profit(svc, test_X, test_y, test_win_odds, proba=True)


# print('\n', 'Decision Tree')
# from sklearn import tree

# high_score = 0
# for criterion in ['gini', 'entropy']:
#     for num_tree in range(2, 11):
#         dt = tree.DecisionTreeClassifier(max_depth=num_tree, criterion=criterion)
#         dt.fit(train_X, train_y)

#         score = f1_score(valid_y, dt.predict(valid_X), average='macro')
#         print(score, criterion, num_tree)
#         if score > high_score:
#             high_score = score
#             best_dt = dt
#             best_depth = num_tree
#             best_criterion = criterion

# print(best_criterion, best_depth)
# # print('Training: ', metric(dt, train_X, train_y))
# # print('Valid: ', metric(dt, valid_X, valid_y))
# print('Testing: ', metric(best_dt, test_X, test_y))
# pf = Profit(best_dt, test_X, test_y, test_win_odds, proba=True)

#Export tree structure to PNG format
# from sklearn.externals.six import StringIO  
# import pydot
# dotfile = StringIO()
# tree.export_graphviz(dt, out_file=dotfile)
# pydot.graph_from_dot_data(dotfile.getvalue())[0].write_png("tree.png")


# print('\n', 'K Nearest Neigbhor')
# from sklearn.neighbors import KNeighborsClassifier
# #Only consider the nearest neighbor
# high_score = 0
# for n_neighbor in range(2, 21):
#     knn = KNeighborsClassifier(n_neighbors=n_neighbor)
#     knn.fit(train_X, train_y)

#     score = f1_score(valid_y, knn.predict(valid_X), average='macro')
#     print(score, n_neighbor)
#     if score > high_score:
#         high_score = score
#         best_knn = knn
#         best_neighbors = n_neighbor

# print(best_neighbors)
# # print('Training: ', metric(knn, train_X, train_y))
# # print('Valid: ', metric(knn, valid_X, valid_y))
# print('Testing: ', metric(best_knn, test_X, test_y))
# pf = Profit(best_knn, test_X, test_y, test_win_odds, proba=True)


print('\n', 'Random Forest')
from sklearn.ensemble import RandomForestClassifier

high_score = 0
for criterion in ['gini', 'entropy']:
    for num_tree in range(2, 11):
        rf = RandomForestClassifier(max_depth=num_tree, criterion=criterion)
        rf.fit(train_X, train_y)

        score = f1_score(valid_y, rf.predict(valid_X), average='macro')
        print(score, criterion, num_tree)
        if score > high_score:
            high_score = score
            best_rf = rf
            best_depth = num_tree
            best_criterion = criterion

print(best_depth, best_criterion)
# print('Training: ', metric(rf, train_X, train_y))
# print('Valid: ', metric(rf, valid_X, valid_y))
print('Testing: ', metric(best_rf, test_X, test_y))
pf = Profit(best_, test_X, test_y, test_win_odds, proba=True)

